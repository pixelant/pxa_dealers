plugin.tx_pxadealers {
	view {
		# cat=plugin.tx_pxadealers/file; type=string; label=Path to template root (FE)
		templateRootPath = EXT:pxa_dealers/Resources/Private/Templates/
		# cat=plugin.tx_pxadealers/file; type=string; label=Path to template partials (FE)
		partialRootPath = EXT:pxa_dealers/Resources/Private/Partials/
		# cat=plugin.tx_pxadealers/file; type=string; label=Path to template layouts (FE)
		layoutRootPath = EXT:pxa_dealers/Resources/Private/Layouts/
	}
	persistence {
		# cat=plugin.tx_pxadealers//a; type=string; label=Default storage PID
		storagePid =
	}
	# cat=plugin.tx_pxadealers//b; type=string; label=Google Geolocation API KEY
	geoLocationApiKey = 
}