# Dealers #

## Good to know: ##
* If address is not valid (found through Google maps search) it won't appear.
* When a dealer is saved the coordinates are considered outdated and forces a fetch from Google Maps API. If address is not valid, it will disappear.
* "I believe on golfstore i addded an option to set static location. Then it is not dependent on the address." /Pavlo

## Pxa dealers Categories plugin: ##

Filters dealers by category.

Options:
* Choose categories to filter on: self explanatory
* Choose categories which will be checked by default: self explanatory

## Pxa dealers countries plugin: ##

Filters dealers by country, state or search by city or zip code

Options:
* Enable country selector : self explanatory
* Enable states selector : self explanatory
* Enable dynamic search for city/zip : self explanatory

## Pxa dealers results: ##

Shows markers on the map. Also shows info cards list with dealers information beneath the map. On page load browser will ask if the user allows geopositioning. If yes - only limited amount of nearest dealers will be shown, otherwise - all dealers.

Options:
* Map height : self explanatory
* Limit of results : amount of results to show if user allowed geoposition
* Display result in rows. Enter amount of items in one row : In other words - number of columns for info-cards list
* Additional class for plugin wrapper in html: self explanatory
* Additional class for wrapper of list of elements : self explanatory
* Additional class for list element: self explanatory
* Enable markers clusterization: enabling this will group markers on the map if they location close to each other.
* Show dealer logo: self explanatory
* Logo width: self explanatory
* Logo height : self explanatory
* Extend city zone by : lat/lng number to extend searched city zone.
* Extend zip zone by : lat/lng number to extend searched zip code zone.